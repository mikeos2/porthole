#!/usr/bin/env python3

"""
    Porthole Main Window
    The main interface the user will interact with

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2003 - 2008
    Fredrik Arnerup, Brian Dolbec,
    Daniel G. Taylor, Wm. F. Wheeler, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    --------------------------------------------------------------------------

    This is the main loop of the program. A lot of changes here, especially
    with glade removal and switching to GtkBuilder and the general switch to
    Gtk3.
    --------------------------------------------------------------------------
"""

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import GLib
# from gi.repository import GObject
from gettext import gettext as _
from porthole.utils import utils
from porthole.backends import portagelib as portage_lib
# from porthole.config import Config
from porthole.config import Paths
# portage_lib = backends.portage_lib
# World = portage_lib.settings.get_world
from porthole.utils.dispatcher import Dispatcher
from porthole.dialogs.about import AboutDialog
from porthole.dialogs.command import RunDialog
from porthole.dialogs.simple import SingleButtonDialog, YesNoDialog
from porthole.dialogs.configure import ConfigDialog
from porthole.packagebook.notebook import PackageNotebook
from porthole.terminal.terminal import ProcessManager
from porthole.views.category import CategoryView
from porthole.views.package import PackageView, PACKAGES, SEARCH, UPGRADABLE, DEPRECATED, SETS  # , BLANK, TEMP
from porthole.views.models import MODEL_ITEM as PACKAGE_MODEL_ITEM
from porthole.advancedemerge.advemerge import AdvancedEmergeDialog
from porthole.readers.upgradeables import UpgradableListReader
# from porthole.readers.descriptions import DescriptionReader
from porthole.readers.deprecated import DeprecatedReader
from porthole.readers.search import SearchReader
from porthole.readers.sets import SetListReader
from porthole.loaders.loaders import *
# from porthole.backends.version_sort import ver_match
from porthole.backends.utilities import get_sync_info
from porthole import db
import datetime
import logging

logger = logging.getLogger(__name__)
logger.debug("MAINWINDOW: id initialized to %s", datetime.datetime.now().microsecond)

""" ****************************************************************************
Defines
**************************************************************************** """
# filter view defines
SHOW_ALL = 0
SHOW_INSTALLED = 1
SHOW_SEARCH = 2
SHOW_UPGRADE = 3
SHOW_DEPRECATED = 4
SHOW_SETS = 5

INDEX_TYPES = ["All", "Installed", "Search", "Upgradable", "Deprecated", "Sets"]
GROUP_SELECTABLE = [SHOW_UPGRADE, SHOW_DEPRECATED, SHOW_SETS]
ON = True
OFF = False

# create the translated reader type names
READER_NAMES = {"Deprecated": _("Deprecated"), "Sets": _("Sets"), "Upgradable": _("Upgradable")}

""" ************************************************************************ """


# noinspection PyAttributeOutsideInit
class MainWindow:
    """Main Window class to setup and manage main window interface."""

    def __init__(self):
        logger.debug("MAINWINDOW: process id = %d ****************", os.getpid())

        # setup glade
        self._DATA_PATH = Paths.get_data_path()
        self.gladefile = self._DATA_PATH + 'glade/main_window.glade'
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)  # loads all objects defined in main_window.glade

        # ***** put in __int__ to quiet pycharm
        # init some dictionaries
        self.loaded_callback = {}
        self.current_cat_name = {}
        self.current_cat_cursor = {}
        self.current_pkg_name = {}
        self.current_pkg_cursor = {}
        self.current_pkg_path = {}
        self.pkg_list = {}
        self.pkg_count = {}
        self.loaded = {}

        # setup prefs
        Config.Prefs.myarch = portage_lib.get_arch()
        logger.debug("MAINWINDOW: Prefs.myarch = %s", Config.Prefs.myarch)

        # register callbacks
        handlers = {
            # main window close
            "on_main_window_destroy": self.goodbye,
            # Action pulldown menu
            "on_emerge_package": self.emerge_btn,
            "on_adv_emerge_package": self.adv_emerge_btn,
            "on_unmerge_package": self.unmerge_btn,
            "on_sync_tree": self.sync_tree,
            "on_upgrade_packages": self.upgrade_packages,
            "on_open_log": self.open_log,
            "on_run_custom": self.custom_run,
            "on_reload_db": self.reload_db,
            "on_re_init_portage": self.re_init_portage,
            "on_quit1_activate": self.quit,
            # Settings pulldown menu
            "on_search_descriptions1_activate": self.search_set,
            "on_configure_porthole": self.configure_porthole,
            # Help pulldown menu
            "on_help_contents": self.help_contents,
            "on_about": self.about,
            # ribbon bar
            "on_view_refresh": self.reload_view,
            "view_filter_changed": self.view_filter_changed,
            "on_package_search": self.package_search,
            "on_search_entry_activate": self.package_search,
            "on_root_warning_clicked": self.check_for_root,
            # Bottom bar
            "on_cancel_btn": self.on_cancel_btn,
            # these are missing handlers: I just deadend these - log&return
            # ToDo figure out what what to do with these
            "on_btn_queue_clicked": self.missing_handler,
            "on_notebook_switch_page": self.missing_handler,
            "on_fetch_activate": self.missing_handler,
        }
        self.builder.connect_signals(handlers)

        self.set_statusbar2("Starting")

        # self.statusbar = self.builder.get_object("statusbar2")
        # self.progressbar = self.builder.get_object("progressbar")

        """ *************************************************************
        aliases for convenience
        
        save the mainwindow widget to Config for use by other modules
        as a parent window
        *************************************************************"""
        self.mainwindow = self.builder.get_object("main_window")  # .get_object("main_window")
        Config.Mainwindow = self.mainwindow

        # main_window -                              self.builder
        #    vpane -
        #       hpane -
        #          category_scrolled_window         result below
        #          package_scrolled_window          result below
        #       notebook                            self.packagebook ---> see notebook.py

        callbacks = {
            "action_callback": self.action_callback,
            "re_init_portage": self.re_init_portage,
            "set_package_actions_sensitive": self.set_package_actions_sensitive
        }
        # initialize this now cause we need it next
        # self.plugin_package_tabs = {}
        # create the primary package notebook --- packagebook.notebook.py
        logger.debug("MAINWINDOW: create the primary package notebook")
        self.packagebook = PackageNotebook(self.builder, callbacks)
        # set unfinished items to not be sensitive
        # self.builder.get_object("contents2").set_sensitive(False)
        # self.builder.get_object("btn_help").set_sensitive(False)

        # setup the category view
        logger.debug("MAINWINDOW: create category view")
        self.category_view = CategoryView()
        self.category_view.register_callback(self.category_changed)
        cat_scroll = self.builder.get_object("category_scrolled_window")
        cat_scroll.add(self.category_view)

        # setup the package treeview
        self.package_view = PackageView()
        # self.package_view.register_callbacks(self.package_changed,
        # None, self.pkg_path_callback)
        # self.package_view.register_callbacks(self.packageview_callback)
        self.package_view.register_callbacks(self.action_callback)
        pwindow = self.builder.get_object("package_scrolled_window")
        pwindow.add(self.package_view)

        # how should we setup our saved menus?
        settings = ["pretend", "fetch", "update", "verbose", "noreplace", "oneshot", "newuse",
                    "withbdeps"]  # "search_descriptions1"]
        for option in settings:
            widget = self.builder.get_object(option)
            state = getattr(Config.Prefs.emerge, option) or False
            logger.debug("MAINWINDOW: __init__(); option = %s, state = %s", option, str(state))
            # if option:
            if state:
                label = option.capitalize()  # add * to active option
                widget.get_child().set_label(label)
                widget.activate()
                widget.connect("activate", self.emerge_setting_set, option)

        # setup a convienience tuple for menu actions
        self.tool_widgets = ["emerge_package1", "adv_emerge_package1",
                             "unmerge_package1", "btn_emerge", "btn_adv_emerge",
                             "btn_unmerge", "btn_sync", "view_refresh", "view_filter"]
        self.widget = {}
        for x in self.tool_widgets:
            self.widget[x] = self.builder.get_object(x)
            if not self.widget[x]:
                logger.debug("MAINWINDOW: __init__(); Failure to obtain widget '%s", x)
        # get an empty tooltip
        # #self.synctooltip = Gtk.Tooltips()
        self.sync_tip = _(
            " Synchronise Package Database \n The last sync was done:\n")
        # set the sync label to the saved one set in the options
        self.widget["btn_sync"].set_label(Config.Prefs.globals.Sync_label)
        self.widget["view_refresh"].set_sensitive(False)

        # restore last window width/height
        if Config.Prefs.main.xpos and Config.Prefs.main.ypos:
            self.mainwindow.move(Config.Prefs.main.xpos, Config.Prefs.main.ypos)
        self.mainwindow.resize(Config.Prefs.main.width, Config.Prefs.main.height)
        # connect gtk callback for window movement and resize events
        self.mainwindow.connect("configure-event", self.size_update)
        # restore maximized state and set window-state-event
        # handler to keep track of it
        if Config.Prefs.main.maximized:
            self.mainwindow.maximize()
        # window-state-event mainwindow signal connection
        self.mainwindow.connect("window-state-event", self.on_window_state_event)
        # move horizontal and vertical panes
        # logger.debug("MAINWINDOW: __init__() before hpane; " +
        # "%d, vpane; %d"
        # %(Config.Prefs.main.hpane, Config.Prefs.main.vpane))
        logger.debug("MAINWINDOW: vpane and hpane")
        self.hpane = self.builder.get_object("vpane")
        self.hpane.set_position(Config.Prefs.main.hpane)
        self.hpane.connect("notify", self.on_pane_notify)
        self.vpane = self.builder.get_object("hpane")
        self.vpane.set_position(Config.Prefs.main.vpane)
        self.vpane.connect("notify", self.on_pane_notify)
        # Intercept the window delete event signal
        # ##self.mainwindow.connect('delete-event', self.confirm_delete)
        # initialize some variable to fix the hpane jump bug
        # self.hpane_bug_count = 0
        # self.hpane_bug = True
        # initialize now so that the update_db_callback doesn't puke
        # self.plugin_manager = None
        # self.plugin_package_tabs = {}
        #
        # initialize our data
        #
        self.init_data()

        # set if we are root or not
        self.is_root = utils.is_root()
        logger.debug("MAINWINDOW: __init__(); is_root = %s", str(self.is_root))
        logger.debug("MAINWINDOW: Nag Dialog %s", Config.Prefs.main.show_nag_dialog)
        if Config.Prefs.main.show_nag_dialog:
            # let the user know if he can emerge or not
            self.check_for_root()
        self.toolbar_expander = self.builder.get_object("toolbar_expander")
        # This should be set in the glade file, but doesn't seem to work ?
        self.toolbar_expander.set_expand(True)
        # create and start our process manager
        self.process_manager = ProcessManager(utils.environment(), False)
        # populate the view_filter menu
        # self.widget["view_filter_list"] \
        filterlist = [_("All Packages"), _("Installed Packages"), _("Search Results"),
                      _("Upgradable Packages"), _("Deprecated Packages"), _("Sets")]
        self.filtercombo = self.builder.get_object("view_filter")
        for filtername in filterlist:
            self.filtercombo.append_text(filtername)

        # ToDo watch --- triggers populate early
        # filtercombo.set_active(0)
        # filtercombo.new_with_model_and_entry(filterstore).set_entry_text_column(0)
        #    self.widget["view_filter_list"].append([i])
        # self.widget["view_filter"].set_model(self.widget["view_filter_list"])
        # self.widget["view_filter"].set_active(SHOW_ALL)
        logger.debug("MAINWINDOW: Showing main window")
        self.mainwindow.show_all()
        if self.is_root:
            # hide warning toolbar widget
            logger.debug("MAINWINDOW: __init__(); hiding btn_root_warning")
            self.builder.get_object("btn_root_warning").hide()

    def init_data(self):
        """initialize the db and anything else related to package selection"""
        # set things we can't do unless a package is selected to not sensitive
        logger.debug("MAINWINDOW: init_data(); Initializing data")
        self.set_package_actions_sensitive(False)
        # set status
        # self.set_statusbar(_("Obtaining package list "))
        self.status_root = _("Loading database")
        self.set_statusbar2(_("Initializing database. Please wait..."))
        self.progressbar = self.builder.get_object("progressbar")
        self.set_cancel_btn(OFF)
        #
        #  db.db.set_callback sets callback in Database()
        #
        db.db.set_callback(self.update_db_read)  # <-- DB callback

        for i in ["All", "Installed", "Upgradable", "Deprecated",
                  "Search", "Sets"]:
            self.current_cat_name[i] = None
            self.current_cat_cursor[i] = None
            self.current_pkg_name[i] = None
            self.current_pkg_cursor[i] = None
            self.current_pkg_path[i] = None
            if i not in ["All", "Installed"]:
                # init pkg lists, counts
                self.pkg_list[i] = {}
                self.pkg_count[i] = {}
                self.loaded[i] = False
            if i in ["Upgradable", "Deprecated", "Sets"]:
                self.loaded_callback[i] = None

        # next add any index names that need to be reset on a reload
        self.loaded_resets = ["Search", "Deprecated"]
        self.current_search = None
        # descriptions loaded?
        # self.desc_loaded = False
        # view filter setting
        self.last_view_setting = None
        # set notebook tabs to load new package info
        self.packagebook.reset_tabs()
        self.reader_running = False
        self.reader = None
        # load the db
        # logger.debug("MAINWINDOW: init_db(); starting db.db.db_thread")
        self.reload = False
        self.upgrade_view = False
        # self.db_timeout = gobject.timeout_add(100, self.update_db_read)
        self.last_sync = _("Unknown")
        self.valid_sync = False
        self.get_sync_time()
        self.set_sync_tip()
        self.new_sync = False
        self.reload_depth = 0
        logger.debug("MAINWINDOW: init_data(); Finished")

    def reload_db(self, *widget):
        """initiatiate a full db reload"""
        logger.debug("MAINWINDOW: reload_db() callback")
        self.progress_done(True)
        for x in self.loaded_resets:
            self.loaded[x] = False
        for i in ["All", "Installed"]:
            self.current_pkg_path[i] = None
        self.current_pkg_cursor["Search"] = None
        # test to reset portage
        # portage_lib.reload_portage()
        portage_lib.settings.reload_world()
        self.upgrade_view = False
        self.get_sync_time()
        self.set_sync_tip()
        # load the db
        # self.dbtime = 0
        db.db.db_init(self.new_sync)
        # test = 87/0  # used to test pycrash is functioning
        self.reload = True
        self.new_sync = False
        # set status
        # self.set_statusbar(_("Obtaining package list "))
        self.status_root = _("Reloading database")
        self.set_statusbar2(self.status_root)
        return False

    def reload_view(self, *widget):
        """reload the category and package view"""
        logger.debug("MAINWINDOW: reload_view populate.")
        if self.widget["view_filter"].get_active() == SHOW_UPGRADE:
            self.loaded["Upgradable"] = False
        else:
            self.category_view.populate(db.db.categories.keys())
        # self.package_view.clear()  ToDo removed
        self.set_package_actions_sensitive(False, None)
        # update the views by calling view_filter_changed
        self.view_filter_changed(self.widget["view_filter"])
        # self.widget["view_refresh"].set_sensitive(False)
        return False

    def package_update(self, pkg):
        """callback function to update an individual package
            after a successfull emerge was detected"""
        # find the pkg in db.db
        db.db.update(portage_lib.extract_package(pkg))

    def sync_callback(self):
        """re-initializes portage so it uses the new metadata cache
           then init our db"""
        # self.re_init_portage()
        portage_lib.settings.reset()
        # self.reload==False is currently broken for init_data when
        # reloading after a sync
        # self.init_data()
        self.new_sync = True
        self.reload_db()
        self.refresh()

    def get_sync_time(self):
        """gets and returns the timestamp info saved during
           the last portage tree sync"""
        self.last_sync, self.valid_sync = get_sync_info()

    def set_sync_tip(self):
        """Sets the sync tip for the new or old toolbar API"""
        self.widget["btn_sync"].set_has_tooltip(True)
        self.widget["btn_sync"].set_tooltip_text(' '.join([self.sync_tip,
                                                           self.last_sync[:], '']))

    def action_callback(self, action=None, arg=None):
        """dispatcher interface callback to handle various actions."""
        logger.debug("MAINWINDOW: action_callback(); caller = %s, action = '%s', arg = %s",
                     arg['caller'], str(action), str(arg))
        # need to check all actions - I do know package.py calls from _clicked() with action ==
        # "package changed" for notebook population.
        if action in ["adv_emerge", "set path", "package changed", "refresh"]:
            # handle possible spaces in callback action string
            _action = action.replace(' ', '_')

            # _action_adv_emerge_(self, arg)
            # _action_set_path_(self, arg)
            # _action_package_changed_(self, arg)
            # _action_refresh_(self, arg)
            ret_val = getattr(self, "_action_%s_" % _action)(arg)
            if ret_val:
                return ret_val
        else:
            return self._action_install(action, arg)

    def _action_adv_emerge_(self, arg):
        """handle advanced emerge action callback"""
        if 'package' in arg:
            package = arg['package']
        elif 'full_name' in arg:
            package = db.db.get_package(arg['full_name'])
        else:
            logger.debug(
                "MAINWINDOW: _action_adv_emerge_(); did not get an expected arg variable for 'adv_emerge' action arg = ",
                str(arg))
            return False
        self.adv_emerge_package(package)
        return True

    def _action_set_path_(self, arg):
        """handle a path setting callback"""
        # save the path to the package that matched the name passed
        # to populate() in PackageView... (?)
        x = self.widget["view_filter"].get_active()
        self.current_pkg_path[x] = arg['path']  # arg = path

    def _action_package_changed_(self, arg):
        """handle a package changed callback"""
        self.package_changed(arg['package'])
        # print(arg['package'].full_name)
        return True

    def _action_refresh_(self, arg):
        """handle a refresh action callback"""
        self.refresh()
        return True

    """
    _action_install only called once from action_callback ~473
    
    possible actions:
    "emerge", "emerge pretend", "unmerge", "set path", "package changed" or "refresh"
    and possibly one argument
    
    
    !!!! Wrong - "set path", "package changed" or "refresh" handled in action_callback
    
    """

    def _action_install(self, action, arg=None):
        """handle install commnad callbacks"""
        print("_action_install starts: " + action)
        print(arg)
        cp = None
        commands = []
        old_pretend_value = Config.Prefs.emerge.pretend
        old_verbose_value = Config.Prefs.emerge.verbose

        print("old_pretend_value = " + str(Config.Prefs.emerge.pretend))
        print("old_verbose_value = " + str(Config.Prefs.emerge.verbose))

        if "emerge" in action:
            commands = ["emerge "]
        elif "unmerge" in action:
            commands = ["emerge --unmerge "]
        if "pretend" in action:
            Config.Prefs.emerge.pretend = True
        else:
            Config.Prefs.emerge.pretend = False
        if "sudo" in action:
            commands = ['sudo -p "Password: " '] + commands

        commands.append(Config.Prefs.emerge.get_string())
        print(Config.Prefs.emerge.get_string())
        if "ebuild" in arg:
            commands.append('=' + arg['ebuild'])
            cp = portage_lib.pkgsplit(arg['ebuild'])[0]
        elif 'package' in arg:
            if arg['package'] is not None:
                cp = arg['package']
                name = cp.full_name
                commands.append(name)
        elif 'full_name' in arg:
            cp = arg['full_name']
            commands.append(arg['full_name'])
        else:
            logger.debug("MAINWINDOW action_callback(): unknown arg '%s'", str(arg))
            return False
        print("_action_install ", commands)
        if cp == None:
            print("Mainwindow _action_install get_name NONE!!!!")
        else:
            print("Mainwindow _action_install get_name " + cp)
        self.setup_command(portage_lib.get_name(cp), ''.join(commands))
        Config.Prefs.emerge.pretend = old_pretend_value
        Config.Prefs.emerge.verbose = old_verbose_value
        return True

    def check_for_root(self, *args):
        """figure out if the user can emerge or not..."""

        if Config.Prefs.main.show_nag_dialog is False:
            return

        if not self.is_root:
            self.no_root_dialog = SingleButtonDialog(
                _("No root privileges"),
                self.mainwindow,
                _("In order to access all the features of Porthole,\nplease run it with root privileges."
                  ), self.remove_nag_dialog,
                _("_Ok"))

    def remove_nag_dialog(self, widget, response):
        """ Remove the nag dialog and set it to not display next time """
        self.no_root_dialog.destroy()
        Config.Prefs.main.show_nag_dialog = False

    """ Located in main_window.glade - GtkStatusbar """

    def set_statusbar2(self, to_string):

        # print(to_string)
        """Update the statusbar without having to use push and pop."""
        statusbar2 = self.builder.get_object("statusbar2")
        statusbar2.pop(0)
        statusbar2.push(0, to_string)

    def update_db_read(self, args):  # extra args for dispatcher callback
        """Update the statusbar according to the number of packages read."""
        # logger.debug("MAINWINDOW: update_db_read()")
        # print(args)
        if args["done"] is False:
            self._update_db_statusbar(args)
        elif args['db_thread_error']:
            # todo: display error dialog instead
            self.set_statusbar2(args['db_thread_error'].decode('ascii',
                                                               'replace'))
            return False  # disconnect from timeout
        else:  # args["done"] == True - db_thread is done
            self._update_db_done()
        # logger.debug("MAINWINDOW: returning from update_db_read() " +
        # "count=%d dbtime=%d"  %(count, self.dbtime))
        return True

    def _update_db_statusbar(self, args):
        """update the statusbar on progress"""
        count = args["nodecount"]
        if count > 0:
            self.set_statusbar2(_("%(base)s: %(count)i packages read")
                                % {'base': self.status_root, 'count': count})
        try:
            fraction = min(1.0, max(0, (count / float(args["allnodes_length"]))))
            progresstxt = str(int(fraction * 100)) + "%"

            self.progressbar.set_text(progresstxt)
            self.progressbar.set_fraction(fraction)
        except:
            pass

    def _update_db_done(self):
        """performs the finalizing and cleanup after the
        db reader is finished"""
        self.progressbar.set_text("100%")
        self.progressbar.set_fraction(1.0)
        self.set_statusbar2(_("%(base)s: Populating tree")
                            % {'base': self.status_root})
        self.update_statusbar(SHOW_ALL)
        logger.debug("MAINWINDOW: _update_db_done(); " +
                     "setting menubar,toolbar,etc to sensitive...")
        for x in ["menubar", "toolbar", "view_filter", "search_entry",
                  "btn_search", "view_refresh"]:
            self.builder.get_object(x).set_sensitive(True)
        # make sure we search again if we reloaded!
        mode = self.widget["view_filter"].get_active()
        logger.debug("MAINWINDOW: _update_db_done() mode = %s type = %s", str(mode), str(type(mode)))
        if mode in [SHOW_SEARCH]:
            # logger.debug("MAINWINDOW: _update_db_done()... Search view")
            # update the views by calling view_filter_changed
            self.view_filter_changed(self.widget["view_filter"])
            # reset the upgrades list if it is loaded and not being viewed
            self.loaded["Upgradable"] = False
            if self.reload:
                # reset _last_selected so it
                # thinks this package is new again
                self.package_view._last_selected = None
                if self.current_pkg_cursor["Search"] is not None \
                        and self.current_pkg_cursor["Search"][0]:
                    # should fix a type error in set_cursor
                    # re-select the package
                    self.package_view.set_cursor(
                        self.current_pkg_cursor["Search"][0],
                        self.current_pkg_cursor["Search"][1])
        elif self.reload and mode in [SHOW_ALL, SHOW_INSTALLED]:
            logger.debug("MAINWINDOW: _update_db_done()... self.reload= True ALL or INSTALLED view")
            # reset _last_selected so it thinks this category is new again
            self.category_view._last_category = None
            # logger.debug("MAINWINDOW: _update_db_done(); re-select the category: self.current_cat_cursor["All_Installed"] =")
            # logger.debug(self.current_cat_cursor["All_Installed"])
            # logger.debug(type(self.current_cat_cursor["All_Installed"]))
            if (self.current_cat_cursor[INDEX_TYPES[mode]] != None) and \
                    (self.current_cat_cursor[INDEX_TYPES[mode]] != [None, None]):
                # re-select the category
                try:
                    self.category_view.set_cursor(
                        self.current_cat_cursor[INDEX_TYPES[mode]][0],
                        self.current_cat_cursor[INDEX_TYPES[mode]][1])
                except:
                    logger.debug("MAINWINDOW: _update_db_done(); error " +
                                 "converting self.current_cat_cursor[%s][]: %s",
                                 str(mode), str(self.current_cat_cursor[INDEX_TYPES[mode]]))
            # ~ #logger.debug("MAINWINDOW: _update_db_done(); reset _last_selected so it thinks this package is new again")
            self.package_view._last_selected = None
            # ~ #logger.debug("MAINWINDOW: _update_db_done(); re-select the package")
            # re-select the package
            if self.current_pkg_path[INDEX_TYPES[mode]] is not None:
                if self.current_pkg_cursor[INDEX_TYPES[mode]] is not None and \
                        self.current_pkg_cursor[INDEX_TYPES[mode]][0]:
                    self.package_view.set_cursor(
                        self.current_pkg_path[INDEX_TYPES[mode]],
                        self.current_pkg_cursor[INDEX_TYPES[mode]][1])
            self.view_filter_changed(self.widget["view_filter"])
            # reset the upgrades list if it is loaded and not being viewed
            self.loaded["Upgradable"] = False
        else:
            if self.reload:
                # logger.debug("MAINWINDOW: _update_db_done()... " +
                # "must be an Upgradable view")
                self.widget['view_refresh'].set_sensitive(True)
                ## hmm, don't mess with upgrade list
                ## after an emerge finishes.
            else:
                logger.debug("MAINWINDOW:_update_db_done(); " +
                             "db_thread is done, reload_view()")
                # need to wait until all other events are done for it to
                # show when the window first opens
                GLib.idle_add(self.reload_view)
        logger.debug("MAINWINDOW: _update_db_done(); " +
                     "Made it thru a reload, returning...")
        self.progress_done(False)
        # if not self.reload:
        # self.view_filter_changed(self.widget["view_filter"])
        self.reload = False
        return False  # disconnect from timeout

    def setup_command(self, package_name, command, run_anyway=False):
        """Setup the command to run or not"""
        if (self.is_root
                or run_anyway
                or (Config.Prefs.emerge.pretend
                    and not command.startswith(Config.Prefs.globals.Sync))
                or command.startswith("sudo ")
                or utils.pretend_check(command)):
            if command.startswith('sudo -p "Password: "'):
                logger.debug('MAINWINDOW: setup_command(); removing ' +
                             '\'sudo -p "Password: "\' for pretend_check')
                is_pretend = utils.pretend_check(command[21:])
            else:
                is_pretend = utils.pretend_check(command)
            logger.debug("MAINWINDOW: setup_command(); emerge.pretend = " +
                         "%s, pretend_check = %s, help_check = %s, info_check = %s",
                         (str(Config.Prefs.emerge.pretend), str(is_pretend),
                          str(utils.help_check(command)),
                          str(utils.info_check(command))))
            if (Config.Prefs.emerge.pretend
                    or is_pretend
                    or utils.help_check(command)
                    or utils.info_check(command)):
                # temp set callback for testing
                # callback = self.sync_callback
                callback = lambda: None  # a function that does nothing
                logger.debug("MAINWINDOW: setup_command(); " +
                             "callback set to lambda: None")
            elif package_name == "Sync Portage Tree":
                callback = self.sync_callback  # self.init_data
                logger.debug("MAINWINDOW: setup_command(); " +
                             "callback set to self.sync_callback")
            else:
                # logger.debug("MAINWINDOW: setup_command(); " +
                # "setting callback()")
                callback = self.reload_db
                logger.debug("MAINWINDOW: setup_command(); " +
                             "callback set to self.reload_db")
                # callback = self.package_update
            # ProcessWindow(command, env, Config.Prefs, callback)
            self.process_manager.add(package_name, command, callback,
                                     _("Porthole Main Window"))
        else:
            logger.debug("MAINWINDOW: Must be root user to run command '%s' ",
                         command)
            # self.sorry_dialog=utils.SingleButtonDialog(_("You are not root!"),
            #        self.mainwindow,
            #        _("Please run Porthole as root to emerge packages!"),
            #        None, "_Ok")
            print("oh shit")
            self.check_for_root()  # displays not root dialog
            return False
        return True

    def emerge_setting_set(self, widget, option='null'):
        """Set whether or not we are going to use an emerge option"""
        logger.debug("MAINWINDOW: emerge_setting_set(%s)", option)
        logger.debug("MAINWINDOW: emerge_setting_set; %s  %s",
                     str(widget), option)
        setattr(Config.Prefs.emerge, option, widget.get_active())
        # Config.Prefs.emerge.oneshot = widget.get_active()

    def search_set(self, widget):
        """Set whether or not to search descriptions"""
        Config.Prefs.main.search_desc = widget.get_active()

    def emerge_btn(self, widget, sudo=False):
        """callback for the emerge toolbutton and menu entries"""
        package = utils.get_treeview_selection(self.package_view, 2)
        self.emerge_package(package, sudo)

    def emerge_package(self, package, sudo=False):
        """Emerge the package."""
        if (sudo or (not utils.is_root() and utils.can_sudo())) \
                and not Config.Prefs.emerge.pretend:
            self.setup_command(package.get_name(),
                               'sudo -p "Password: " emerge' +
                               Config.Prefs.emerge.get_string() +
                               package.full_name)
        else:
            self.setup_command(package.get_name(), "emerge" +
                               Config.Prefs.emerge.get_string() + package.full_name)

    def adv_emerge_btn(self, *widget):
        """Advanced emerge of the currently selected package."""
        package = utils.get_treeview_selection(self.package_view, 2)
        self.adv_emerge_package(package)

    def adv_emerge_package(self, package):
        """Advanced emerge of the package."""
        # Activate the advanced emerge dialog window
        # re_init_portage callback is for when package.use etc. are modified
        dialog = AdvancedEmergeDialog(package, self.setup_command,
                                      self.re_init_portage)

    def configure_porthole(self, menuitem_widget):
        """Shows the Configuration GUI"""
        config_dialog = ConfigDialog()

    def unmerge_btn(self, widget, sudo=False):
        """callback for the Unmerge button and menu entry to
        unmerge the currently selected package."""
        package = utils.get_treeview_selection(self.package_view, 2)
        self.unmerge_package(package, sudo)

    def unmerge_package(self, package, sudo=False):
        """Unmerge the package."""
        if (sudo or (not self.is_root and utils.can_sudo())) \
                and not Config.Prefs.emerge.pretend:
            self.setup_command(package.get_name(),
                               'sudo -p "Password: " emerge --unmerge' +
                               Config.Prefs.emerge.get_string() + package.full_name)
        else:
            self.setup_command(package.get_name(), "emerge --unmerge" +
                               Config.Prefs.emerge.get_string() + package.full_name)

    def sync_tree(self, *widget):
        """Sync the portage tree and reload it when done."""

        # sync method in configuration
        sync = Config.Prefs.globals.Sync
        if Config.Prefs.emerge.verbose:
            sync += " --verbose"
        if Config.Prefs.emerge.nospinner:
            sync += " --nospinner "

        command = "pkexec /bin/bash -c '" + sync + " >/dev/null && exit@'"

        from porthole.utils.cmdspawn import Command
        cmd_run = Command(command)
        cmd_run.start()

        print("moving on")

        sync_win = Gtk.Window(title="Sync...")
        sync_win.set_default_size(300,200)
        sync_win.set_position(Gtk.WindowPosition.CENTER)
        spinner = Gtk.Spinner()
        label = Gtk.Label("\nSync'ing database, please wait ...")
        grid = Gtk.Grid()
        grid.add(label)
        grid.attach_next_to(spinner, label, Gtk.PositionType.BOTTOM, 1, 2)
        grid.set_row_homogeneous(True)
        sync_win.add(grid)
        sync_win.show_all()
        # sync_win.start()
        spinner.start()
        # spinner.stop()
        # sync_win.destroy()
        """
        if utils.is_root():
            self.setup_command("Sync Portage Tree", sync)
        elif utils.can_sudo():
            self.setup_command("Sync Portage Tree", 'sudo -p "Password: " ' +
                               sync)
        
        else:
            self.check_for_root()
        """
        # pkexec /bin/bash -c 'emerge --sync && read -s -n 1 -p "Press any key to continue . . ." && exit'
        #subprocess.run(["pkexec /bin/bash -c 'emerge --sync && read -s -n 1 -p \"Press any key to continue . . .\" && exit'"], shell = true)
        # process = subprocess.run(['ls', '-lha'], check=True, stdout=subprocess.PIPE, universal_newlines=True)

    def on_cancel_btn(self, *widget):
        """cancel button callback function"""
        logger.debug("MAINWINDOW: on_cancel_btn() callback")
        # terminate the thread
        self.reader.please_die()
        self.reader.join()
        self.progress_done(True)

    def on_window_state_event(self, widget, event):
        """Handler for window-state-event gtk callback.
        Just checks if the window is maximized or not"""
        if widget is not self.mainwindow:
            return False
        logger.debug("MAINWINDOW: on_window_state_event(); event detected")
        if Gdk.WindowState.MAXIMIZED & event.new_window_state:
            Config.Prefs.main.maximized = True
        else:
            Config.Prefs.main.maximized = False

    def on_pane_notify(self, pane, gparamspec):
        """callback function for the pane re-size signal
        stores the new settings for next time"""
        if gparamspec.name == "position":
            # save hpane, vpane positions
            Config.Prefs.main.hpane = self.hpane.get_position()
            Config.Prefs.main.vpane = self.vpane.get_position()

    def get_selected_list(self):
        """creates self.packages_list, self.keyorder"""
        logger.debug("MAINWINDOW: get_selected_list()")
        my_type = INDEX_TYPES[self.last_view_setting]
        if self.last_view_setting not in GROUP_SELECTABLE:
            logger.debug("MAINWINDOW: get_selected_list() " + my_type +
                         " view is not group selectable for emerge/upgrade commands")
            return False
        # create a list of packages to be upgraded
        self.packages_list = {}
        self.keyorder = []
        if self.loaded[my_type]:
            logger.debug("MAINWINDOW: get_selected_list() %s  loaded", my_type)
            self.list_model = self.package_view.view_model[my_type]
            # read the my_type tree into a list of packages
            logger.debug("MAINWINDOW: get_selected_list(); run list_model.foreach() len = %s",
                         str(len(self.list_model)))
            logger.debug("MAINWINDOW: get_selected_list(); self.list_model = %s", str(self.list_model))
            self.list_model.foreach(self.tree_node_to_list)
            logger.debug("MAINWINDOW: get_selected_list(); " +
                         "list_model.foreach() Done")
            logger.debug("MAINWINDOW: get_selected_list(); " +
                         "len(self.packages_list) = " + str(len(self.packages_list)))
            logger.debug("MAINWINDOW: get_selected_list(); self.keyorder) = " +
                         str(self.keyorder))
            return len(self.keyorder) > 0
        else:
            logger.debug("MAINWINDOW: get_selected_list() %s not loaded", my_type)
            return False

    def upgrade_packages(self, *widget):
        """Upgrade selected packages that have newer versions available."""
        if self.last_view_setting in GROUP_SELECTABLE:
            if not self.get_selected_list():
                logger.debug("MAINWINDOW: upgrade_packages(); " +
                             "No packages were selected")
                return
            logger.debug("MAINWINDOW: upgrade_packages(); " +
                         "packages were selected")
            if self.is_root or Config.Prefs.emerge.pretend:
                emerge_cmd = "emerge "
            elif utils.can_sudo():
                emerge_cmd = 'sudo -p "Password: " emerge '
            else:  # can't sudo, not root
                # display not root dialog and return.
                self.check_for_root()
                return
            # logger.debug(self.packages_list)
            # logger.debug(self.keyorder)
            for key in self.keyorder:
                if not self.packages_list[key].in_world:
                    logger.debug("MAINWINDOW: upgrade_packages(); " +
                                 "dependancy selected: ", key)
                    options = Config.Prefs.emerge.get_string()
                    if "--oneshot" not in options:
                        options = options + " --oneshot "
                    if not self.setup_command(key, emerge_cmd +
                                                   options + key[:]):  # use the full name
                        return
                elif not self.setup_command(key,
                                            emerge_cmd + Config.Prefs.emerge.get_string() +
                                            ' ' + key[:]):
                    # use the full name
                    return
        else:
            logger.debug("MAIN: Upgrades not loaded; upgrade world?")
            self.upgrades_loaded_dialog = YesNoDialog(_("Upgrade requested"),
                                                      self.mainwindow,
                                                      _("Do you want to upgrade all packages in your world file?"),
                                                      self.upgrades_loaded_dialog_response)

    def tree_node_to_list(self, model, path, _iter):
        """callback function from Gtk.TreeModel.foreach(),
           used to add packages to the self.packages_list,
           self.keyorder lists"""
        # logger.debug("MAINWINDOW; tree_node_to_list(): begin building list")
        if model.get_value(_iter, PACKAGE_MODEL_ITEM["checkbox"]):
            name = model.get_value(_iter, PACKAGE_MODEL_ITEM["name"])
            logger.debug("MAINWINDOW; tree_node_to_list(): name '%s'", name)
            if name not in self.keyorder \
                    and name != _("Upgradable dependencies:"):
                self.packages_list[name] = model.get_value(_iter,
                                                           PACKAGE_MODEL_ITEM["package"])
                # model.get_value(_iter, PACKAGE_MODEL_ITEM["world"])
                # model.get_value(_iter, PACKAGE_MODEL_INDEX["package"]), name]
                self.keyorder = [name] + self.keyorder
                logger.debug("MAINWINDOW; tree_node_to_list(): new keyorder list = %s", str(self.keyorder))
        return False

    def upgrades_loaded_dialog_response(self, widget, response):
        """ Get and parse user's response """
        if response == 0:  # Yes was selected; upgrade all
            # self.load_upgrades_list()
            # self.loaded_callback["Upgradable"] = self.upgrade_packages
            if not utils.is_root() and utils.can_sudo() \
                    and not Config.Prefs.emerge.pretend:
                self.setup_command('world',
                                   'sudo -p "Password: " emerge --update' +
                                   Config.Prefs.emerge.get_string() + 'world')
            else:
                self.setup_command('world', "emerge --update" +
                                   Config.Prefs.emerge.get_string() + 'world')
        else:
            # load the upgrades view to select which packages
            self.widget["view_filter"].set_active(SHOW_UPGRADE)
        # get rid of the dialog
        self.upgrades_loaded_dialog.destroy()

    def load_descriptions_list(self):
        """ Load a list of all descriptions for searching """
        self.desc_dialog = SingleButtonDialog(_("Please Wait!"),
                                              self.mainwindow,
                                              _("Loading package descriptions..."),
                                              self.desc_dialog_response, "_Cancel", True)
        logger.debug("MAINWINDOW: load_descriptions_list(); " +
                     "starting self.desc_thread")
        db.db.load_descriptions()
        db.db.set_desc_callback(self.desc_thread_update)

    def desc_dialog_response(self, widget, response):
        """ Get response from description loading dialog """
        # kill the update thread
        db.db.cancell_desc_update()
        self.desc_dialog.destroy()

    def desc_thread_update(self, args):
        """ Update status of description loading process """
        if args['done']:
            if not args['cancelled']:
                # search with descriptions
                self.package_search(None)
            self.desc_dialog.destroy()
            return False
        else:
            # print self.desc_thread.count
            if args['count']:
                fraction = args['count'] / float(len(db.db.list))
                self.desc_dialog.progbar.set_text(
                    str(int(fraction * 100)) + "%")
                self.desc_dialog.progbar.set_fraction(fraction)
        return True

    def package_search(self, widget=None):
        """Search package db with a string and display results."""
        self.clear_package_detail()
        if not db.db.desc_loaded and Config.Prefs.main.search_desc:
            self.load_descriptions_list()
            return
        tmp_search_term = self.builder.get_object("search_entry").get_text()
        # logger.debug(tmp_search_term)
        if tmp_search_term:
            # change view and statusbar so user knows it's searching.
            # This won't actually do anything unless we thread the search.
            self.loaded["Search"] = True  # or else
            # v_f_c() tries to call package_search again
            self.widget["view_filter"].set_active(SHOW_SEARCH)
            if Config.Prefs.main.search_desc:
                self.set_statusbar2(_("Searching descriptions for %s")
                                    % tmp_search_term)
            else:
                self.set_statusbar2(_("Searching for %s") % tmp_search_term)
            # call the thread
            self.search_thread = SearchReader(db.db.list,
                                              Config.Prefs.main.search_desc, tmp_search_term,
                                              db.db.descriptions, Dispatcher(self.search_done))
            self.search_thread.start()
        return

    # start of search callback
    def search_done(self):
        """show the search results from the search thread"""
        logger.debug("MAINWINDOW: search_done populate.")
        # if self.search_thread.done:
        if not self.search_thread.cancelled:
            # grab the list
            package_list = self.search_thread.package_list
            count = self.search_thread.pkg_count
            search_term = self.search_thread.search_term
            # kill off the thread
            self.search_thread.join()
        # in case the search view was already active
        self.update_statusbar(SHOW_SEARCH)
        self.pkg_list["Search"][search_term] = package_list
        self.pkg_count["Search"][search_term] = count
        # Add the current search item & select it
        self.category_view.populate(self.pkg_list["Search"].keys(), True,
                                    self.pkg_count["Search"])
        _iter = self.category_view.model.get_iter_first()
        while _iter != None:
            if self.category_view.model.get_value(_iter, 1) == search_term:
                selection = self.category_view.get_selection()
                selection.select_iter(_iter)
                break
            _iter = self.category_view.model.iter_next(_iter)
        self.package_view.populate(package_list)
        if count == 1:  # then select it
            self.current_pkg_name["Search"] = package_list.keys()[0]
        self.category_view.last_category = search_term
        self.category_changed(search_term)

    def help_contents(self, *widget):
        """Show the help file contents."""
        webbrowser.open('file://' + Config.Prefs.DATA_PATH + 'help/index.html')

    def about(self, *widget):
        """Show about dialog."""
        dialog = AboutDialog()

    def refresh(self):
        """Refresh PackageView"""
        logger.debug("MAINWINDOW: refresh()")
        mode = self.widget["view_filter"].get_active()
        if mode in [SHOW_SEARCH]:
            self.category_changed(self.current_search)
        else:
            self.category_changed(self.current_cat_name[INDEX_TYPES[mode]])

    def category_changed(self, category):
        """Catch when the user changes categories."""
        mode = self.widget["view_filter"].get_active()
        # log the new category for reloads
        self._remember_selected(mode, category)
        if not self.reload:
            self.current_pkg_cursor["All"] = None
            self.current_pkg_cursor["Installed"] = None
        # logger.debug("Category cursor = " +
        # str(self.current_cat_cursor["All_Installed"]))
        # logger.debug("Category = " + category)
        # logger.debug(self.current_cat_cursor["All_Installed"][0])#[1])
        (cursor, sub_row) = self._get_cursor(mode)

        # so ... in this case we clicked in the category window, now I need to clear
        # and reload the package window ...
        self.clear_package_detail()
        if not category or sub_row:
            logger.debug('MAINWINDOW: category_changed(); category=False or ' +
                         'self.current_cat_cursor[%s][0][1]==None', INDEX_TYPES[mode])
            packages = None
            self.current_pkg_name[INDEX_TYPES[mode]] = None
            self.current_pkg_cursor[INDEX_TYPES[mode]] = None
            self.current_pkg_path[INDEX_TYPES[mode]] = None
            # self.package_view.set_view(PACKAGES)
            self.package_view.populate(None)
        elif mode in [SHOW_UPGRADE, SHOW_DEPRECATED, SHOW_SETS,
                      SHOW_SEARCH, SHOW_ALL, SHOW_INSTALLED]:
            # call the correct mode function to handle the change
            getattr(self, '_mode_%s_' % INDEX_TYPES[mode].lower())(category, mode)
        else:
            # nice ... must not have been a good day MKG
            raise Exception("The programmer is stupid. " +
                            "Unknown category_changed() mode")

    def _mode_search_(self, category, mode):
        packages = self.pkg_list["Search"][category]
        # if search was a package name, select that one
        # (searching for 'python' for example would benefit)
        """
        ToDo is causing:
        Traceback (most recent call last):
        File "/home/mgreene/SharedVBox/PycharmProjects/porthole/porthole/views/category.py", line 130, in _clicked
        self._category_changed(category)
        File "/home/mgreene/SharedVBox/PycharmProjects/porthole/porthole/mainwindow.py", line 1160, in category_changed
        getattr(self, '_mode_%s_' % INDEX_TYPES[mode].lower())(category, mode)
        File "/home/mgreene/SharedVBox/PycharmProjects/porthole/porthole/mainwindow.py", line 1168, in _mode_search_
        packages = self.pkg_list["Search"][category]
        KeyError: 'virtual' or other KeyError
        """
        self.package_view.populate(packages, category)

    def _mode_readers_(self, category, mode):
        packages = self.pkg_list[INDEX_TYPES[mode]][category]  # ToDo getting random errors here KeyError:
        self.package_view.populate(packages,
                                   self.current_pkg_name[INDEX_TYPES[mode]])

    def _mode_all_(self, category, mode):
        packages = db.db.categories[category]
        self.package_view.populate(packages,
                                   self.current_pkg_name["All"])

    def _mode_installed_(self, category, mode):
        try:
            packages = db.db.installed[category]
            self.package_view.populate(packages, self.current_pkg_name["Installed"])
        except:
            print("Error _mode_installed_")
        # self.package_view.populate(packages, self.current_pkg_name["Installed"])  # ToDo uncertain fix here

    def _mode_upgradable_(self, catoegory, mode):
        self._mode_readers_(catoegory, mode)

    def _mode_deprecated_(self, catoegory, mode):
        self._mode_readers_(catoegory, mode)

    def _mode_sets_(self, catoegory, mode):
        print("in mode sets")
        print(catoegory)
        print(mode)
        self._mode_readers_(catoegory, mode)

    def _remember_selected(self, mode, category):
        """Store the selected category and package for the mode"""
        if mode in [SHOW_SEARCH]:
            self.current_search = category
            self.current_search_cursor = self.category_view.get_cursor()
        elif mode not in [SHOW_SEARCH]:  # , SHOW_UPGRADE, SHOW_SETS]:
            self.current_cat_name[INDEX_TYPES[mode]] = category
            self.current_cat_cursor[INDEX_TYPES[mode]] = \
                self.category_view.get_cursor()
        # ~ elif mode == SHOW_UPGRADE:
        # ~ self.current_cat_name["All_Installed"]["Upgradable"] = category
        # ~ self.current_upgrade_cursor = self.category_view.get_cursor()

    def _get_cursor(self, mode):
        """returns the previous cursor and subrow
        selections for mode"""
        if self.current_cat_cursor[INDEX_TYPES[mode]]:
            cursor = self.current_cat_cursor[INDEX_TYPES[mode]][0]
            if cursor and len(cursor) > 1:
                sub_row = cursor[1] == None
            else:
                sub_row = False
        else:
            cursor = None
            sub_row = False
        return cursor, sub_row

    def clear_package_detail(self):
        """tells packagebook to clear itself
        sets the package actions options off"""
        # called early when filter view (Combo Box) changes
        self.packagebook.clear_notebook()
        self.set_package_actions_sensitive(False)

    def package_changed(self, package):
        """Catch when the user changes packages."""
        logger.debug("MAINWINDOW: package_changed()")
        if not package or package.full_name == _("None"):
            self.clear_package_detail()
            self.current_pkg_name[INDEX_TYPES[0]] = ''
            self.current_pkg_cursor[INDEX_TYPES[0]] = self.package_view.get_cursor()
            self.current_pkg_path[INDEX_TYPES[0]] = self.current_pkg_cursor[INDEX_TYPES[0]][0]
            return
        # log the new package for db reloads
        x = self.widget["view_filter"].get_active()
        self.current_pkg_name[INDEX_TYPES[x]] = package.get_name()
        self.current_pkg_cursor[INDEX_TYPES[x]] = self.package_view.get_cursor()
        # xxx remove logger.debug("MAINWINDOW: package_changed(); new cursor = %d", str(self.current_pkg_cursor[INDEX_TYPES[x]]))
        self.current_pkg_path[INDEX_TYPES[x]] = self.current_pkg_cursor[INDEX_TYPES[x]][0]
        # logger.debug("Package name= " +
        # "%s, cursor = " %str(self.current_pkg_name[INDEX_TYPES[x]]))
        # logger.debug(self.current_pkg_cursor[INDEX_TYPES[x]])
        # the notebook must be sensitive before anything is displayed
        # in the tabs, especially the deps_view
        self.set_package_actions_sensitive(True, package)
        self.packagebook.set_package(package)

    # myview:
    # SHOW_ALL, SHOW_INSTALLED
    # SHOW_SEARCH
    # SHOW_UPGRADE, SHOW_DEPRECATED, SHOW_SETS
    def view_filter_changed(self, widget):
        """Update the treeviews for the selected filter
        # This is the handler for the Combo Box
        # trigger: "GtkComboBoxText" id="view_filter"
        # name="changed" handler="view_filter_changed"

        Okay why this here? I found that the filtercombo.set_active(0) statement early on
        triggered view_filter_changed when no data is present to display. So, made filtercombo
        a class var which results is its state and no activating it. This way the initial state
        being -1, I catch it here, activate the filter view and return. I think this works and
        prevents a no-data run through populate -- but I could have caused an issue, verify

        NOTHING = -1
        SHOW_ALL = 0
        SHOW_INSTALLED = 1
        SHOW_SEARCH = 2
        SHOW_UPGRADE = 3
        SHOW_DEPRECATED = 4
        SHOW_SETS = 5
        """
        myview = widget.get_active()
        if myview is -1:
            self.filtercombo.set_active(0)
            return

        logger.debug("MAINWINDOW: view_filter_changed(); myview = %d", myview)
        self.update_statusbar(myview)

        # cat window object (left) and ?? search False
        cat_scroll = self.builder.get_object("category_scrolled_window")
        self.category_view.set_search(False)

        # clear and turn off package window (right)
        self.clear_package_detail()
        cat = None
        pack = None
        sort_categories = False

        # see above for defines
        # handle All Packages and Installed filter view
        if myview in (SHOW_INSTALLED, SHOW_ALL):
            if myview == SHOW_ALL:
                self.category_view.populate(db.db.categories.keys(), True, db.db.pkg_count)
            else:
                self.category_view.populate(db.db.installed.keys(), True, db.db.installed_pkg_count)
            # load window
            cat_scroll.show()

            logger.debug("MAINWINDOW: view_filter_changed(); reset package_view")
            self.package_view.set_view(PACKAGES)
            logger.debug("MAINWINDOW: view_filter_changed(); init package_view")
            self.package_view._init_view()
            logger.debug("MAINWINDOW: view_filter_changed(); clear package_view")
            # self.package_view.clear()
            cat = self.current_cat_name[INDEX_TYPES[myview]]
            pack = self.current_pkg_name[INDEX_TYPES[myview]]
            logger.debug("MAINWINDOW: view_filter_changed(); reselect category & package")

        # handle Search filter view
        elif myview == SHOW_SEARCH:
            self.category_view.set_search(True)
            if not self.loaded[INDEX_TYPES[myview]]:
                self.set_package_actions_sensitive(False, None)
                self.category_view.populate(
                    self.pkg_list[INDEX_TYPES[myview]].keys(),
                    True,
                    self.pkg_count[INDEX_TYPES[myview]])
                self.package_search(None)
                self.loaded[INDEX_TYPES[myview]] = True
            else:
                self.category_view.populate(
                    self.pkg_list[INDEX_TYPES[myview]].keys(),
                    True,
                    self.pkg_count[INDEX_TYPES[myview]])
            cat_scroll.show()
            logger.debug("MAIN: Showing search results")
            self.package_view.set_view(SEARCH)
            cat = self.current_search
            pack = self.current_pkg_name[INDEX_TYPES[myview]]

        # handle Upgrade, Deprecated, and Set filter view
        elif myview in [SHOW_UPGRADE, SHOW_DEPRECATED, SHOW_SETS]:
            logger.debug("MAINWINDOW: view_filter_changed(); '%s' selected", INDEX_TYPES[myview])
            cat_scroll.show()
            # all need to be sorted for them to be
            # displayed in the tree correctly
            sort_categories = True

            # set view option:
            if myview == SHOW_UPGRADE:
                self.package_view.set_view(UPGRADABLE)  # package.py _init_view _set_model
            elif myview == SHOW_DEPRECATED:
                self.package_view.set_view(DEPRECATED)
            else:
                self.package_view.set_view(SETS)

            # INDEX_TYPES Upgradable/Deprecated/Search/Sets
            # will be set as True/False - first run all will be false
            if not self.loaded[INDEX_TYPES[myview]]:
                self.load_reader_list(INDEX_TYPES[myview])  # load upgrade reader
                # self.package_view.clear()
                # self.category_view.clear()  ToDo WTF
                logger.debug("MAINWINDOW: view_filter_changed(); back from "
                             "load_reader_list('%s')", INDEX_TYPES[myview])
            else:
                logger.debug("MAINWINDOW: view_filter_changed(); calling "
                             "category_view.populate() with categories: %s",
                             str(self.pkg_list[INDEX_TYPES[myview]].keys()))
                self.category_view.populate(
                    self.pkg_list[INDEX_TYPES[myview]].keys(),
                    sort_categories,
                    self.pkg_count[INDEX_TYPES[myview]])
            # self.package_view.set_view(UPGRADABLE)
            logger.debug("MAINWINDOW: view_filter_changed(); init package_view")
            self.package_view._init_view()
            # logger.debug("MAINWINDOW: view_filter_changed(); " +
            # "clear package_view")
            # self.package_view.clear()
            cat = self.current_cat_name[INDEX_TYPES[myview]]
            pack = self.current_pkg_name[INDEX_TYPES[myview]]

        # Below old source -- remove later
        # ~ elif myview == SHOW_SETS:
        # ~ logger.debug("MAINWINDOW: view_filter_changed(); Sets selected")
        # ~ cat = None #self.current_cat_name["All_Installed"]
        # ~ pack = None #self.current_pkg_name["All_Installed"]
        # ~ pass

        logger.debug("MAINWINDOW: view_filter_changed(); " +
                     "reselect category & package (maybe)")
        # ***** End of filter view handling *****

        if cat != None:  # and pack != None:
            self.select_category_package(cat, pack)
        # clear the notebook tabs
        # self.clear_package_detail()
        # if self.last_view_setting != myview:
        logger.debug("MAINWINDOW: view_filter_changed(); last_view_setting  %s" +
                     " changed: myview = %s", str(self.last_view_setting), str(myview))
        self.last_view_setting = myview
        # self.current_cat_name["All_Installed"] = None
        # self.category_view.last_category = None
        # self.current_cat_cursor["All_Installed"] = None
        # self.current_pkg_cursor["All_Installed"] = None

    def select_category_package(self, cat, pack):
        """method to re-select the same category and package that
        were selected last time in this view

        @param cat: category string to re-select
        @param pack package to re-select
        """
        logger.debug("MAINWINDOW: select_category_package(): " +
                     "%s/%s", cat, pack)
        catpath = None
        if cat and '-' in cat:
            # find path of category
            catpath = self._find_catpath(cat)
        elif cat:
            model = self.category_view.get_model()
            _iter = model.get_iter_first()
            while _iter:
                if cat == model.get_value(_iter, 0):
                    catpath = model.get_path(_iter)
                    break
                _iter = model.iter_next(_iter)
        #    catpath = 'Sure, why not?'
        else:
            logger.debug("MAINWINDOW: select_category_package(): bad category?")
        if catpath:
            self.category_view.expand_to_path(catpath)
            self.category_view.last_category = None  # so it thinks it's changed
            self.category_view.set_cursor(catpath)
            # now reselect whatever package we had selected
            path = self._find_pkgpath(pack)
            if not path:
                logger.debug("MAINWINDOW: select_category_package(): " +
                             "no package found")
                self.clear_package_detail()
        else:
            logger.debug("MAINWINDOW: select_category_package(): " +
                         "no category path found")
            self.clear_package_detail()

    def _find_catpath(self, cat):
        """Find the path for the category

        @param cat: category string
        """
        catpath = None

        model = self.category_view.get_model()
        _iter = model.get_iter_first()
        catmaj, catmin = cat.split("-", 1)
        logger.debug("MAINWINDOW: _find_catpath(); catmaj, catmin = %s, %s",
                     catmaj, catmin)
        while _iter:
            logger.debug("value at iter %s: %s",
                         iter, model.get_value(_iter, 0))
            if catmaj == model.get_value(_iter, 0):
                (catpath, kiditer) = self._find_catmin(_iter, catmin, model)
                if catpath:
                    logger.debug("MAINWINDOW: _find_catpath(); " +
                                 "found value at iter %s: %s",
                                 _iter, model.get_value(kiditer, 0))
                    break
            _iter = model.iter_next(_iter)
        return catpath

    def _find_catmin(self, _iter, catmin, model):
        """Find the minor category belonging to the parent @ _iter"""
        kids = model.iter_n_children(_iter)
        while kids:  # this will count backwards, but hey so what
            kiditer = model.iter_nth_child(_iter, kids - 1)
            if catmin == model.get_value(kiditer, 0):
                catpath = model.get_path(kiditer)
                break
            kids -= 1
        return catpath, kiditer

    def _find_pkgpath(self, pack):
        """"""
        model = self.package_view.get_model()
        _iter = model.get_iter_first()
        path = None
        while _iter and pack:
            # logger.debug("value at _iter %s: %s"
            # % (_iter, model.get_value(_iter, 0)))
            if model.get_value(_iter, 0).split('/')[-1] == pack:
                path = model.get_path(_iter)
                self.package_view._last_selected = None
                self.package_view.set_cursor(path)
                break
            _iter = model.iter_next(_iter)
        return path

    def load_reader_list(self, reader):
        """multipurpose loader can run a number of different
        reader threads"""
        self.reader_progress = 1
        # package list is not loaded, create dialog and load them
        self.set_statusbar2(_("Generating '%s' packages list...") % READER_NAMES[reader])
        # create reader thread for loading the packages
        if self.reader_running:
            logger.debug("MAINWINDOW: load_reader_list(); " +
                         "thread already running!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            return
        logger.debug("MAINWINDOW: load_reader_list(); starting thread %s", reader)

        # reader branching
        if reader == "Deprecated":
            self.reader = DeprecatedReader(db.db.installed.items())
        elif reader == "Upgradable":
            self.reader = UpgradableListReader(db.db.installed.items())
        elif reader == "Sets":
            self.reader = SetListReader()

        # start selected reader thread
        self.reader.start()
        self.reader_running = True
        logger.debug("MAINWINDOW: load_reader_list(); reader_running set to True")

        # ToDo What is this for?
        self.build_deps = False

        # add a timeout to check if thread is done
        GLib.timeout_add(200, self.update_reader_thread)
        self.set_cancel_btn(ON)

    def wait_dialog_response(self, widget, response):
        """ Get a response from the wait dialog """
        if response == 0:
            # terminate the thread
            self.reader.please_die()
            self.reader.join()
            # get rid of the dialog
            self.wait_dialog.destroy()

    def update_reader_thread(self):
        """ Find out if thread is finished """
        # needs error checking perhaps...
        reader_type = self.reader.reader_type
        if self.reader.done:
            print("Reader Done True")
            logger.debug("MAINWINDOW: update_reader_thread(): self.reader.done detected")
            return self._reader_done(reader_type)
        elif self.reader.progress < 2:
            print("Reader <2")
            # Still building system package list nothing to do
            pass
        else:
            # statusbar hack,
            # should probably be converted to use a Dispatcher callback
            if self.reader.progress >= 2 and self.reader_progress == 1:
                self.set_statusbar2(_("Searching for '%s' packages...") %
                                    READER_NAMES[self.reader.reader_type])
                self.reader_progress = 2
            if self.reader_running:
                try:
                    if self.build_deps:
                        count = 0
                        for key in self.reader.pkg_count:
                            count += self.reader.pkg_count[key]
                        fraction = count / float(self.reader.pkg_dict_total)
                        self.progressbar.set_text(
                            str(int(fraction * 100)) + "%")
                        self.progressbar.set_fraction(fraction)
                    else:
                        fraction = \
                            self.reader.count / float(db.db.installed_count)
                        self.progressbar.set_text(
                            str(int(fraction * 100)) + "%")
                        self.progressbar.set_fraction(fraction)
                        if fraction == 1:
                            self.build_deps = True
                            self.set_statusbar2(_("Building Package List"))
                except Exception as _error:
                    logger.debug("MAINWINDOW: update_reader_thread(): " +
                                 "Exception: %s", _error)
        return True

    def _reader_done(self, reader_type):
        """perform the cleanup"""
        logger.debug("MAINWINDOW: In _reader_done()")
        self.reader.join()
        self.reader_running = False
        self.progress_done(True)
        if self.reader.cancelled:
            return False
        logger.debug("MAINWINDOW: _reader_done(): reader_type = %s", reader_type)
        if reader_type in ["Upgradable", "Deprecated", "Sets"]:
            self.pkg_list[reader_type] = self.reader.pkg_dict
            self.pkg_count[reader_type] = self.reader.pkg_count
            logger.debug("MAINWINDOW: _reader_done(): pkg_count = %s", str(self.pkg_count))
            self.loaded[reader_type] = True
            self.view_filter_changed(self.widget["view_filter"])
            if self.loaded_callback[reader_type]:
                self.loaded_callback[reader_type](None)
                self.loaded_callback[reader_type] = None
            else:
                if self.last_view_setting == SHOW_UPGRADE:
                    self.package_view.set_view(UPGRADABLE)
                    self.packagebook.summary.update_package_info(None)
                elif self.last_view_setting == SHOW_DEPRECATED:
                    self.package_view.set_view(DEPRECATED)
                    self.packagebook.summary.update_package_info(None)
                elif self.last_view_setting == SHOW_SETS:
                    self.package_view.set_view(SETS)
                    self.packagebook.summary.update_package_info(None)
            return False

    def progress_done(self, button_off=False):
        """clears the progress bar"""
        if button_off:
            self.set_cancel_btn(OFF)
        self.progressbar.set_text("")
        self.progressbar.set_fraction(0)
        self.status_root = _("Done: ")
        self.set_statusbar2(self.status_root)

    def set_cancel_btn(self, state):
        """function name and parameter says it all"""
        self.builder.get_object("btn_cancel").set_sensitive(state)

    def update_statusbar(self, mode):
        """Update the statusbar for the selected filter"""
        text = ""
        if mode == SHOW_ALL:
            if not db.db:
                logger.debug("MAINWINDOW: update_statusbar(); " +
                             "attempted to update with no db assigned")
            else:
                text = (_("%(pack)d packages in %(cat)d categories")
                        % {'pack': len(db.db.list),
                           'cat': len(db.db.categories)})
        elif mode == SHOW_INSTALLED:
            if not db.db:
                logger.debug("MAINWINDOW: update_statusbar(); " +
                             "attempted to update with no db assigned")
            else:
                text = (_("%(pack)d packages in %(cat)d categories")
                        % {'pack': db.db.installed_count,
                           'cat': len(db.db.installed)})
        elif mode in [SHOW_SEARCH, SHOW_DEPRECATED, SHOW_SETS]:
            text = ''
        elif mode == SHOW_UPGRADE:
            if not self.reader:
                logger.debug("MAINWINDOW:  update_statusbar(); " +
                             "attempted to update with no reader thread assigned")
            else:
                text = ''
        self.set_statusbar2(self.status_root + text)

    def set_package_actions_sensitive(self, enabled, package=None):
        """Sets package action buttons/menu items to sensitive or not"""
        logger.debug("MAINWINDOW: set_package_actions_sensitive(%d)", enabled)
        self.widget["emerge_package1"].set_sensitive(enabled)
        self.widget["adv_emerge_package1"].set_sensitive(enabled)
        self.widget["unmerge_package1"].set_sensitive(enabled)
        self.widget["btn_emerge"].set_sensitive(enabled)
        self.widget["btn_adv_emerge"].set_sensitive(enabled)
        if not enabled or enabled and package.get_installed():
            # logger.debug("MAINWINDOW: set_package_actions_sensitive() " +
            # "setting unmerge to %d" %enabled)
            self.widget["btn_unmerge"].set_sensitive(enabled)
            self.widget["unmerge_package1"].set_sensitive(enabled)
        else:
            # logger.debug("MAINWINDOW: set_package_actions_sensitive() " +
            # "setting unmerge to %d" %(not enabled))
            self.widget["btn_unmerge"].set_sensitive(not enabled)

            self.widget["unmerge_package1"].set_sensitive(not enabled)
        self.packagebook.notebook.set_sensitive(enabled)

    def size_update(self, widget, event):
        # logger.debug("MAINWINDOW: size_update(); called.")
        """ Store the window and pane positions """
        Config.Prefs.main.width = event.width
        Config.Prefs.main.height = event.height
        pos = widget.get_position()
        # note: event has x and y attributes but they
        # do not give the same values as get_position().
        Config.Prefs.main.xpos = pos[0]
        Config.Prefs.main.ypos = pos[1]
        return

    def open_log(self, widget):
        """ Open a log of a previous emerge in a new terminal window """
        newterm = ProcessManager(utils.environment(), True)
        newterm.do_open(widget, None)
        return

    def custom_run(self, *widget):
        """ Run a custom command in the terminal window """
        # logger.debug("MAINWINDOW: entering custom_run")
        # logger.debug(Config.Prefs.run_dialog.history)
        RunDialog(self.setup_command, run_anyway=True)
        return

    def re_init_portage(self, *widget):
        """re-initializes the imported portage modules in order to
        see changines in any config files
        e.g. /etc/make.conf USE flags changed"""
        portage_lib.reload_portage()
        self.view_filter_changed(self.widget["view_filter"])
        return

    def quit(self, widget):
        """
        GtkImageMenuItem - quit1 - menu quit
        Confirms there are no running processes before
        calling the gooodbye()"""
        logger.debug("MAINWINDOW: menuitem quit() ")
        if not self.confirm_delete():
            self.goodbye(None)

    def goodbye(self, *widget):
        """
        GtkWindow destory signal window X
        Main window quit function"""
        logger.debug("MAINWINDOW: goodbye(); quiting now")
        Gtk.main_quit()

    def confirm_delete(self, *widget, **event):
        """Check that there are no running processes
        & confirm the kill before doing it"""
        if self.process_manager.task_completed:
            self.process_manager.allow_delete = True
            return False
        err = _("Confirm: Kill the Running Process in the Terminal")
        dialog = Gtk.MessageDialog(self.mainwindow, Gtk.DialogFlags.MODAL,
                                   Gtk.MessageType.QUESTION,
                                   Gtk.ButtonsType.YES_NO, err)
        result = dialog.run()
        dialog.destroy()
        if result != Gtk.ResponseType.YES:
            logger.debug("TERMINAL: kill(); not killing")
            return True
        # self.process_manager.confirm = False
        if self.process_manager.kill_process(None, False):
            logger.debug("MAINWINDOW: process killed, destroying window")
            self.process_manager.allow_delete = True
            self.process_manager.window.hide()
        return False

    def missing_handler(self, widget, pointer, index):
        logger.debug("MAINWINDOW: Missing handler")
        return
