#!/usr/bin/env python3

"""
    Porthole changelog loader function

    Copyright (C) 2020 July - Michael Greene

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import urllib.request
from bs4 import BeautifulSoup
import multiprocessing as mp
# from porthole.backends import portagelib as portage_lib


def fetchchangelog(queue):
    """ Process to get changelog webpage and save it """
    token = "/tmp/reading.porthole"
    file = open(token, 'w')
    file.close()
    cp = queue.get()
    changeurl = "https://gitweb.gentoo.org/repo/gentoo.git/atom/" + cp + "?h=master"
    data = urllib.request.urlopen(changeurl).read()
    raw = BeautifulSoup(data, features="lxml").get_text()
    cat_pack = cp.split("/")
    path = "/tmp/" + cat_pack[0] + "_" + cat_pack[1] + ".porthole"
    file = open(path, 'w')
    file.write(raw)
    file.close()
    if os.path.isfile(token):
        os.remove(token)
    return


class ChangeLogProcess:
    """ Start process to retrieve changelog """
    def __init__(self, package):
        if package.full_name is None:
            return
        self.queue = mp.Queue()
        self.chlogproc = mp.Process(target=fetchchangelog, args=(self.queue,))
        self.chlogproc.start()
        self.queue.put(package.full_name)
