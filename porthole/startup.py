#!/usr/bin/env python3

"""
    Porthole
    A graphical frontend to Portage

    Fixed for Python 3 - April 2020 Michael Greene

    Copyright (C) 2003 - 2009 Fredrik Arnerup and Daniel G. Taylor,
    Brian Dolbec, William F. Wheeler, Brian Bockelman, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import sys
import datetime
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from pathlib import Path

# Is this still true? - MKG
while '/usr/bin' in sys.path:  # and now importing gtk re-adds it! Grrrr, rude
    sys.path.remove('/usr/bin')

import locale
import gettext


# it is recommended to init threads right after importing gtk just in case
# gtk.threads_init()
# gtk.gdk.threads_init()

def import_error(e):
    print("*** Error loading porthole modules!\n*** If you are running a",
          "local (not installed in python's site-packages) version, please use the '--local'",
          "or '-l' flag.\n",
          "*** Otherwise, verify that porthole was installed correctly and",
          "that python's path includes the site-packages directory.\n",
          "If you have recently updated python, then run 'python-updater'\n")
    print("Your sys.path: %s\n" % sys.path)
    print("Your sys.version: %s\n" % sys.version)
    print("Original exception was: ImportError: %s\n" % e)


def local():
    # if opt in ("-l", "--local"):
    # running a local version (i.e. not installed in /usr/*)
    # print "STARTUP: local(); setting to local paths"
    data_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + "/porthole/"
    return data_path


# this seems to be just a quick check, if we aren't in site-packages then we are indeed
# running local
location = os.path.abspath(__file__)
if "site-packages" not in location:
    local()


# sets up debug - need to revisit the debug_target input in startup args
# right now it is an all or nothing
def set_debug(arg):
    from porthole.utils import debug
    debug.set_debug(True)
    # print "Debug printing is enabled = ", debug.debug, "; debug.id = ", debug.id
    debug.debug_target = arg
    # print("Debug print filter set to ", debug.debug_target)


# routine for -v command line arg
def print_version():
    # print version info
    # from version import version
    from porthole.version import version
    print("Porthole 3: ", version)
    print("Python version = ", sys.version)


# insert path for running installed
def insert_path():
    sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))


def main():
    """start the porthole frontend"""
    global Config, preferences

    from porthole.version import version
    from porthole.config import Paths

    _APP = Paths.get_app_name()
    _DATA_PATH = Paths.get_data_path()
    _BACKENDS = Paths.get_backend()
    _i18n_DIR = Paths.get_i18n_path()
    _LOG_DIR = Paths.get_log_path()
    _LOCAL = Paths.get_run_local()

    # set i18n for local running
    if _LOCAL:
        _i18n_DIR = _DATA_PATH + "i18n/"
        Paths.set_i18n_path(_i18n_DIR)

    # logging to user directory - ~/.config/porthole
    user_home = str(Path.home())
    user_porthole = user_home + "/.config/porthole"

    try:
        os.path.exists(user_porthole)
    except:
        os.mkdir(user_porthole)

    # Temp dir for dev
    dbugdir = user_porthole + "/debug.log"

    """
    Stub to start using python logger
    """
    logger = logging.getLogger('porthole')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    if os.path.exists(dbugdir):
        os.remove(dbugdir)
    fh = logging.FileHandler(dbugdir)
    fh.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(fh)

    logger.debug("STARTUP: id initialized to %d", datetime.datetime.now().microsecond)
    logger.debug("In startup main - Logger started")
    logger.debug("Porthole 3: %s", version)
    logger.debug("Python version = %s", sys.version)

    try:
        logger.debug("STARTUP: main(); ************** importing config/config.preferences **************")
        logger.debug("STARTUP: main(); importing config.preferences")
        from porthole.config import preferences
        from porthole.config import Config
        Config.set_path(_DATA_PATH)
    except ImportError as e:
        import_error(e)
    # load prefs
    prefs_additions = [
        ["DATA_PATH", _DATA_PATH],
        ["APP", _APP],
        ["i18n_DIR", _i18n_DIR],
        ["RUN_LOCAL", _LOCAL],
        ["LOG_FILE_DIR", _LOG_DIR],
        ["PORTAGE", _BACKENDS]
    ]
    logger.debug("STARTUP: main(); ************** loading preferences ************** ")
    Config.Prefs = preferences.PortholePreferences(prefs_additions)
    # logger.debug("STARTUP: main(); importing version")
    # from porthole.version import version
    """
    There really are not any other backends except for portage. The other options,
    pkgcore and dbus, must be left over from past versions (or maybe future stuff ??).
    **** Just remember, portage is the only valid option ****
    """
    logger.debug("STARTUP: main(); ************** loading backend ************** ")
    import porthole.backends
    porthole.backends.load(_BACKENDS)

    from porthole.backends import portagelib

    #ebuild = portagelib.get_version("sys-kernel/gentoo-sources-5.5.6-r1")
    #print(ebuild)
    # print(portagelib.get_best_ebuild("=sys-kernel/gentoo-sources-5.6*"))
    # return
    logger.debug("PORTAGELIB: is it up?")
    # print(portagelib.get_porttree())

    logger.debug("PORTHOLE: importing MainWindow")
    from porthole.mainwindow import MainWindow

    logger.debug("PORTHOLE: i18n_DIR = %s", _i18n_DIR)
    locale.setlocale(locale.LC_ALL, '')
    gettext.bindtextdomain(_APP, _i18n_DIR)
    gettext.textdomain(_APP)
    gettext.install(_APP, _i18n_DIR)

    logger.debug("PORTHOLE: process id = %d ****************", os.getpid())
    # setup our app icon
    myicon = _DATA_PATH + "/pixmaps/porthole-icon.png"
    Gtk.Window.set_default_icon_from_file(myicon)
    # load config info
    Config.load()
    # create the main window
    logger.debug("STARTUP: main(); ************** Create Main Window **************")
    myapp = MainWindow()  # config.Prefs, config.Config)
    # start the program loop
    logger.debug("STARTUP: main(); ************** Main loop starting **************")
    # Gtk.main() triggers on_window_state_event when executed
    # Dispatcher.on_data()
    Gtk.main()
    logger.debug("STARTUP: main(); ************** Main loop ending **************")
    # save the prefs to disk for next time
    Config.Prefs.save()
