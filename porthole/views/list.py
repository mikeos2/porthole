#!/usr/bin/env python3

""" File: porthole/views/hightlight.py
    This file is part of the Porthole, a graphical portage-frontend.

    Fixed for Python 3 - April 2020 Michael Greene

    Copyright (C) 2006-2009  René 'Necoro' Neumann
    This is free software.  You may redistribute copies of it under the terms of
    the GNU General Public License version 2.
    There is NO WARRANTY, to the extent permitted by law.

    Written by  René 'Necoro' Neumann <necoro@necoro.net>
    Adapted to Porthole by, Brian Dolbec <dol-sen@users.sourceforge.net>
"""

import datetime
import logging

logger = logging.getLogger(__name__)
logger.debug("LIST: import id initialized to %d", datetime.datetime.now().microsecond)

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from porthole.views.lazyview import LazyView


# noinspection PyUnresolvedReferences
class ListView(Gtk.TextView, LazyView):

    def __init__(self, get_file_fn):

        logger.debug("LISTVIEW: __init__")
        logger.debug("LISTVIEW: fn: %s", get_file_fn)
        if get_file_fn:
            self.get_fn = get_file_fn
        else:  # assume it is passed a filename already
            self.get_fn = self._get_fn

        Gtk.TextView.__init__(self)
        LazyView.__init__(self)

        self.set_editable(False)
        self.set_cursor_visible(False)

    def _get_fn(self, x):
        return x

    def set_text(self, text):
        logger.debug("LISTVIEW: set_text: %s", text[:max(len(text), 500)])
        self.get_buffer().set_text(text)

    def _get_content(self):
        return self.get_fn(self.pkg)
