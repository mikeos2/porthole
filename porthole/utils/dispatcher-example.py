#! /usr/bin/env python3

"""
    Porthole dispatcher module
    Holds common debug functions for Porthole

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Brian Dolbec

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# Fredrik Arnerup <foo@stacken.kth.se>, 2004-12-19
# Brian Dolbec<dol-sen@telus.net>,2005-3-30


import os
import threading
from time import sleep

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gtk, GObject

from dispatcher import Dispatcher


# ####################################
# dispatcher
# example code:
#
#
# ####################################

class Thread(threading.Thread):

    def __init__(self, dispatcher, thread_num, length):
        threading.Thread.__init__(self)
        self.setDaemon(1)  # quit even if this thread is still running
        self.dispatcher = dispatcher
        self.thread_num = thread_num
        self.sleep_length = length

    def run(self):
        global num
        done = False
        print("thread_num = %s; process id = %d ****************" % (self.thread_num, os.getpid()))
        pid_func(self.thread_num)
        for num in range(250):
            # print self.thread_num, " num = ",num
            sleep(self.sleep_length)
            data = [self.thread_num, (": time is slipping away: %d\n" % num), num, done]
            self.dispatcher(data)  # signal main thread
        done = True
        data = [self.thread_num, (": Time slipped away: I'm done"), num, done]
        self.dispatcher(data)  # signal main thread


def pid_func(threadnum):
    print("pid_func: called from thread_num = %s; process id = %d ****************" % (threadnum, os.getpid()))


def message_fun(textbuffer, message):
    if message[3]:
        thread_finished[message[0]] = True
        textbuffer.insert_at_cursor(message[0] + str(message[1]))
    else:
        # message2 = ("%d x 3 = %d\n" %(message[2],message[2]*3))
        textbuffer.insert_at_cursor(message[0] + str(message[1]))  # + message2)
    return


def timerfunc():
    if (not thread_finished["thread1"]) or (not thread_finished["thread2"]) \
            or (not thread_finished["thread3"]) or (not thread_finished["thread4"]):
        progressbar.pulse()
        # print 'Plusing ProgressBar, since a thread is not finished'
        return True
    else:
        progressbar.set_fraction(0)
        progressbar.set_text("Done")
        return False


def on_window_map_event(event, param):
    print('Window mapped')
    thread1 = Thread(Dispatcher(message_fun, textbuffer), "thread1", 0.9)
    thread2 = Thread(Dispatcher(message_fun, textbuffer), "thread2", 0.9)
    thread3 = Thread(Dispatcher(message_fun, textbuffer), "thread3", 0.9)
    thread4 = Thread(Dispatcher(message_fun, textbuffer), "thread4", 0.5)
    GLib.timeout_add(100, timerfunc)
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()


if __name__ == "__main__":
    # creates an empty window
    window = Gtk.Window(title="Dispatcher Example")
    # print(dir(window.props))

    # generate scrolled window and text view
    scrolledwindow = Gtk.ScrolledWindow()
    textview = Gtk.TextView()
    textview.set_wrap_mode(True)
    textbuffer = textview.get_buffer()
    """
    textbuffer.set_text("This is some text inside of a Gtk.TextView. "
                        + "Select text and click one of the buttons 'bold', 'italic', "
                        + "or 'underline' to modify the text accordingly.")
    buffer = textview.get_buffer()
    """

    # add textview to scrolledwindow
    scrolledwindow.add(textview)

    # generate Box
    vbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
    # print(dir(vbox.props))
    # print(textview.props.buffer)

    # progress bar
    progressbar = Gtk.ProgressBar()
    # print(dir(progressbar.props))

    # Add scrolledwindow and progress bar to Box
    vbox.pack_start(scrolledwindow, True, True, 0)
    vbox.pack_start(progressbar, True, True, 0)

    # Add Box to window
    window.add(vbox)

    """
    https://python-gtk-3-tutorial.readthedocs.io/en/latest/index.html
    connecting to the window’s delete event to ensure that the application 
    is terminated if we click on the x to close the window.
    """
    # interface with Dispatcher Class
    gui_dispatcher = Dispatcher(message_fun, textview)
    thread_finished = {"thread1": False, "thread2": False, "thread3": False, "thread4": False}

    """
    The map_event signal will be emitted when the widget's window is mapped.
    A window is mapped when it becomes visible on the screen.
    """
    window.connect('map_event', on_window_map_event)
    window.connect("destroy", Gtk.main_quit)
    window.resize(800, 600)

    # we display the window
    window.show_all()

    # start the GTK+ processing loop which we quit when the window is closed
    Gtk.main()
