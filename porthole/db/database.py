#!/usr/bin/env python3

"""
    Package Database
    A database of Gentoo's Portage tree

    Fixed for Python 3 - April 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Daniel G. Taylor,
    Wm. F. Wheeler, Brian Dolbec, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import datetime
import logging
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib
from porthole.backends import portagelib as portage_lib
from porthole.db.dbreader import DatabaseReader
from porthole.readers.descriptions import DescriptionReader
from porthole.db.dbbase import DBBase
from porthole.utils.dispatcher import Dispatcher
from porthole.config import Paths

logger = logging.getLogger(__name__)
logger.debug("DB-DATABASE: id initialized to %d", datetime.datetime.now().microsecond)


class Database(DBBase):
    def __init__(self):
        DBBase.__init__(self)
        logger.debug("DATABASE: db __init__(self)")
        self.descriptions = {}
        self.desc_loaded = False
        self.desc_reloaded = False

        self.db = None
        self.db_thread = None
        self.db_thread_running = False  # if thread running - True, not running - False
        self.db_init_new_sync = False
        self.db_init_waiting = False

        self.callback = None
        self.desc_callback = None
        self.desc_thread = None

        # ToDo I don't think following is needed any longer
        self._DBFile = Paths.get_db_path() + "/descriptions.db"
        logger.debug("DATABASE: path: %s", self._DBFile)

        self.valid_sync = False  # used for auto-reload disabling
        self.desc_mtime = 0      # Time of last modification _DBFile

        self.db_init()

    def db_init(self, new_sync=False):
        """
        db_init
        Input:  new_sync  True/False

        DBReader thread control - If thread running stop it, if not
        running start it.  Uses flags for control

        At start - db_thread_running = False so we kick off DatabaseReader with
        new_sync = False
        """
        logger.debug("DATABASE: db_init()")
        if self.db_thread_running:
            logger.debug("DATABASE: db_init()  db_thread_running")
            self.db_thread.join()
            self.db_thread_running = False
            # self.db_thread.please_die()
            # set the init is waiting flag
            self.db_init_waiting = True
            self.db_init_new_sync = new_sync
        else:
            logger.debug("DATABASE: db_init()  NOT db_thread_running")
            self.db_thread_running = True
            self.db_thread = DatabaseReader(Dispatcher(self.db_update))
            self.db_thread.daemon = True
            self.db_thread.start()
            self.db_init_new_sync = False
            if new_sync:
                # force a reload
                self.desc_loaded = False

    def set_callback(self, callback):
        """ mainwindow.py sets this to self.update_db_read """
        self.callback = callback

    def get_package(self, full_name):
        """Get a Package object based on full name."""
        # I thought this was over kill just to pull the Package object from
        # the database, but it is a nice error check just in case
        try:
            logger.debug("DATABASE: get_package(); fullname = %s", full_name)
            category = portage_lib.get_category(full_name)
            name = portage_lib.get_name(full_name)
            if category in self.categories and name in self.categories[category]:
                return self.categories[category][name]
            else:
                if category is not 'virtual':
                    logger.debug(
                        "DATABASE: get_package(); package not found for: %s original full_name = %s", name, full_name)
                return None
        except Exception as e:
            logger.debug("DATABASE: get_package(); exception occured: %s", str(e))
            return None

    def update_package(self, fullname):
        """Update the package info in the full list and the installed list"""
        # category, name = fullname.split("/")
        logger.debug("DATABASE: update_package()")
        category = portage_lib.get_category(fullname)
        name = portage_lib.get_name(fullname)
        if category in self.categories and name in self.categories[category]:
            self.categories[category][name].update_info()
        if category in self.installed and name in self.installed[category]:
            self.installed[category][name].update_info()

    # dbreader thread callback
    #
    def db_update(self, args):  # extra args for dispatcher callback
        """Update the callback to the number of packages read."""
        # args ["nodecount", "allnodes_length","done"]
        logger.debug("DATABASE: db_update() check")
        # Is Dbreader running -- FALSE?
        if args["done"] is False:
            logger.debug("DATABASE: db_update() args %s", args)
            # Has callback been set -- if True then execute callback
            if self.callback:
                self.callback(args)
        elif self.db_thread.error:
            logger.debug("DATABASE: db_update() db_thread.error")
            # todo: display error dialog instead
            self.db_thread.join()
            self.db_thread_running = False
            if self.callback:
                args['db_thread_error'] = self.db_thread.error
                self.callback(args)
            return False  # disconnect from timeout
        elif self.db_thread.cancelled:
            logger.debug("DATABASE: db_update() db_thread.cancelled")
            self.db_thread.join()
            self.db_thread_running = False
        else:  # args["done"] == True - db_thread is done
            logger.debug("DATABASE: db_update() db_thread.done")
            self.db_thread_running = False
            if self.callback:
                self.callback(args)
            logger.debug("DATABASE: db_update(); db_thread is done...")
            logger.debug("DATABASE: db_update(); db_thread.join...")
            self.db_thread.join()
            self.db_thread_running = False
            logger.debug("DATABASE: db_update(); db_thread.join is done...")
            # del self.db  # clean up the old db
            self.db = self.db_thread.get_db()
            self.categories = self.db.categories
            self.list = self.db.list
            self.installed = self.db.installed
            self.installed_count = self.db.installed_count
            self.pkg_count = self.db.pkg_count
            self.installed_pkg_count = self.db.installed_pkg_count
            del self.db  # clean up
            logger.debug("DATABASE: db_update(); db is updated")
            self.load_descriptions()

        if self.db_init_waiting:
            self.db_init_waiting = False
            self.db_init(self.db_init_new_sync)

    def load_descriptions(self):
        logger.debug("DATABASE: load_descriptions()")
        if not self.desc_loaded:  # At start desc_loaded is false to force a load
            # At some point an attempt to load the description from a db file
            # which was deadended, so a load happens at start
            self.desc_thread = DescriptionReader(self.list)
            self.desc_thread.start()
            GLib.timeout_add(100, self.desc_thread_update)

    def cancell_desc_update(self):
        if self.desc_thread:
            self.desc_thread.please_die()
            self.desc_thread.join()

    def set_desc_callback(self, callback):
        self.desc_callback = callback

    def desc_thread_update(self):
        """ Update status of description loading process """
        callback_args = {}
        # the below fills log - use w/caution
        # logger.debug("DATABASE: desc_thread_update()")
        if self.desc_callback:
            # gather  the callback data
            callback_args = {'cancelled': self.desc_thread.cancelled,
                             'done': self.desc_thread.done,
                             'count': self.desc_thread.count}
        if self.desc_thread.done:
            # grab the db
            self.descriptions = self.desc_thread.descriptions
            if not self.desc_thread.cancelled:
                self.desc_loaded = True
                self.desc_reloaded = True
                if self.desc_callback:
                    self.desc_callback(callback_args)
                # kill off the thread
            self.desc_thread.join()
            logger.debug("DATABASE: desc_thread_update(); Done")
            return False
        else:
            if self.desc_callback:
                self.desc_callback(callback_args)
        return True
